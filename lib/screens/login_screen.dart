import 'dart:async';

import 'package:alcodes_on_board_flutter/app_router.dart';
import 'package:alcodes_on_board_flutter/constants/app_constants/app_constants.dart'
    as appConst;
import 'package:alcodes_on_board_flutter/constants/shared_preference_keys.dart';
import 'package:alcodes_on_board_flutter/dialogs/app_alert_dialog.dart';
import 'package:alcodes_on_board_flutter/models/form_models/sign_in_form_model.dart';
import 'package:alcodes_on_board_flutter/repository/auth_repo.dart';
import 'package:alcodes_on_board_flutter/utils/app_focus_helper.dart';
import 'package:alcodes_on_board_flutter/utils/check_for_connection.dart';
import 'package:flutter/material.dart';
import 'package:flutter_fimber/flutter_fimber.dart';
import 'package:shared_preferences/shared_preferences.dart';

/// TODO Issue on this screen:
/// - Password field should hide the inputs for security purpose.
/// - Login button is too small, make it fit the form width.
/// - Should show loading when calling API, and hide loading when server responded.
/// - Wrong login credential is showing as Toast, Toast will auto dismiss
///   and user will miss out the error easily, should use pupop dialog.

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final _formKey = GlobalKey<FormState>();
  final _formModel = SignInFormModel();
  int _state = 0;

  Future<void> _onSubmitFormAsync() async {
    bool connectionStatus = null;
    //check for internet connection (return false if no internet)
    connectionStatus = await checkInternet.checkConnection(context);

    if (connectionStatus) {
      // Hide keyboard.
      AppFocusHelper.instance.requestUnfocus();

      // Validate form and save inputs to form model.
      if (_formKey.currentState.validate()) {
        _formKey.currentState.save();

        // Call API.
        var alertDialogStatus = AppAlertDialogStatus.error;
        var alertDialogMessage = '';

        try {
          final repo = AuthRepo();
          final apiResponse = await repo.signInAsync(
            email: _formModel.email,
            password: _formModel.password,
          );

          // Login success, store user credentials into shared preference.
          final sharedPref = await SharedPreferences.getInstance();

          await sharedPref.setString(
              SharedPreferenceKeys.userEmail, _formModel.email);
          await sharedPref.setString(
              SharedPreferenceKeys.userToken, apiResponse.data.token);

          // Navigate to home screen.
          Navigator.of(context)
              .pushNamedAndRemoveUntil(AppRouter.home, (route) => false);

          return;
        } catch (ex) {
          Fimber.e('d;;Error request sign in.', ex: ex);

          alertDialogMessage = '$ex';
        }

        if (alertDialogMessage.isNotEmpty) {
          if (alertDialogMessage.isNotEmpty) {
            // Has message, show alert dialog.
            //dynamic alert dialogue, if allow to show yes and no option when choice is true
            //show ok option when choice is false
            // return true if press yes option, false when press no option
            final appAlertDialog = AppAlertDialog();

            appAlertDialog.showAsync(
              context: context,
              status: alertDialogStatus,
              message: alertDialogMessage,
              choice: false,
            );
          }
        }
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    BuildContext test = context;
    return Scaffold(
      appBar: AppBar(
        title: Text('Login'),
      ),
      body: SingleChildScrollView(
          padding: const EdgeInsets.all(appConst.kDefaultPadding),
          child: _getForm(context)),
    );
  }

  Widget _getForm(BuildContext context) {
    return Form(
      key: _formKey,
      child: Column(
        children: [
          TextFormField(
            initialValue: 'eve.holt@reqres.in',
            keyboardType: TextInputType.emailAddress,
            textInputAction: TextInputAction.next,
            onSaved: (newValue) => _formModel.email = newValue,
            validator: (value) {
              if (value.isEmpty) {
                return 'Required field.';
              }

              return null;
            },
            onFieldSubmitted: (value) => _onSubmitFormAsync(),
            decoration: InputDecoration(
              hintText: 'Email',
            ),
          ),
          _getPadding(),
          TextFormField(
            initialValue: 'cityslicka',
            obscureText: true,
            textInputAction: TextInputAction.done,
            onSaved: (newValue) => _formModel.password = newValue,
            onFieldSubmitted: (value) => _onSubmitFormAsync(),
            validator: (value) {
              if (value.isEmpty) {
                return 'Required field.';
              }

              return null;
            },
            decoration: InputDecoration(
              hintText: 'Password',
            ),
          ),
          _getPadding(),
          SizedBox(
            width: double.infinity,
            child: new MaterialButton(
              color: Colors.blue,
              child: _setUpLoginButton(),
              onPressed: () {
                if (_state == 0) {
                  animateButton();
                }
              },
            ),
          ),
          _getPadding(),
          new InkWell(
              child: Text('Terms and Conditions',
                  style: TextStyle(fontSize: 10, color: Colors.blue)),
              onTap: () => {Navigator.of(context).pushNamed(AppRouter.terms)})
        ],
      ),
    );
  }

  Widget _getPadding() {
    return SizedBox(height: appConst.kDefaultPadding);
  }

  Widget _setUpLoginButton() {
    if (_state == 0) {
      return new Text(
        "Log In",
        style: const TextStyle(
          color: Colors.white,
          fontSize: 16.0,
        ),
      );
    } else if (_state == 1) {
      return SizedBox(
        child: CircularProgressIndicator(
          valueColor: AlwaysStoppedAnimation<Color>(Colors.white),
        ),
        height: 15.0,
        width: 15.0,
      );
    }
  }

  void animateButton() {
    setState(() {
      _state = 1;
    });

    _onSubmitFormAsync().whenComplete(() => () {
          setState(() {});
        });
  }
}
