import 'package:flutter/material.dart';
import 'package:webview_flutter/webview_flutter.dart';

/// TODO Issue on this screen:
/// - Password field should hide the inputs for security purpose.
/// - Login button is too small, make it fit the form width.
/// - Should show loading when calling API, and hide loading when server responded.
/// - Wrong login credential is showing as Toast, Toast will auto dismiss
///   and user will miss out the error easily, should use pupop dialog.

class TermsScreen extends StatefulWidget {
  @override
  _TermsScreenState createState() => _TermsScreenState();
}

class _TermsScreenState extends State<TermsScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Terms and Condition'),
        ),
        body: WebView(
          initialUrl:
              "https://www.termsfeed.com/blog/sample-terms-and-conditions-template/",
          javascriptMode: JavascriptMode.unrestricted,
        ));
  }
}
