import 'package:alcodes_on_board_flutter/constants/app_constants/app_constants.dart'
    as appConst;
import 'package:alcodes_on_board_flutter/models/response_models/friend_details_model.dart';
import 'package:flutter/material.dart';

class MyFriendDetails extends StatefulWidget {
  final FriendDetailsModel data;

  //get the object when user clicked
  //pass object thru app router
  MyFriendDetails({
    Key key,
    @required this.data,
  }) : super(key: key);

  @override
  _MyFriendDetailsState createState() => _MyFriendDetailsState();
}

class _MyFriendDetailsState extends State<MyFriendDetails> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Friend Details'),
      ),
      body: SingleChildScrollView(
        padding: const EdgeInsets.all(appConst.kDefaultPadding),
          child: Column(
            children: [
              SizedBox(height: appConst.kDefaultPadding),
              _CircularAvatar(),
              _FriendDetailsCardView(),
            ],
          ),
      ),
    );
  }

  Widget _CircularAvatar() {
    return CircleAvatar(
      radius: 60.0,
      backgroundImage: NetworkImage(widget.data.avatar),
      backgroundColor: Colors.transparent,
    );
  }

  Widget _FriendDetailsCardView() {
    return Column(children: [
      SizedBox(height: appConst.kDefaultPadding),
      Card(
          elevation: 10,
          child: ListTile(
            title: Text("First Name : \n" + widget.data.first_name),
          )),
      SizedBox(height: appConst.kDefaultPadding),
      Card(
          elevation: 10,
          child: ListTile(
            title: Text("Last Name : \n" + widget.data.last_name),
          )),
      SizedBox(height: appConst.kDefaultPadding),
      Card(
          elevation: 10,
          child: ListTile(
            title: Text("Email : \n" + widget.data.email),
          )),
    ]);
  }
}
