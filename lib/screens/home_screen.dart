import 'package:alcodes_on_board_flutter/app_router.dart';
import 'package:alcodes_on_board_flutter/constants/app_constants/app_constants.dart'
    as appConst;
import 'package:alcodes_on_board_flutter/constants/shared_preference_keys.dart';
import 'package:alcodes_on_board_flutter/models/response_models/friend_details_model.dart';
import 'package:alcodes_on_board_flutter/repository/friend_list_repo.dart';
import 'package:alcodes_on_board_flutter/utils/check_for_connection.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../dialogs/app_alert_dialog.dart';

/// TODO Issue on this screen:
/// - Show confirm logout dialog before logout user.
/// - Go to my friend list screen when click My Friend List button.

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  Future<_DataModel> _futureBuilder;
  _DataModel _dataModel;

  Future<void> _onLogoutButtonPressedAsync() async {
    // Clear data and go to login screen.
    final sharedPref = await SharedPreferences.getInstance();
    await sharedPref.clear();
    Navigator.of(context)
        .pushNamedAndRemoveUntil(AppRouter.login, (route) => false);
  }

  void _onMyFriendListButtonPressed() {
    // Which type of navigator to use?
    Navigator.of(context).pushNamed(AppRouter.myFriendList);
    // Navigator.of(context).pushReplacementNamed(AppRouter.myFriendList);
    // Navigator.of(context).pushNamedAndRemoveUntil(AppRouter.myFriendList, (route) => false);
  }

  void _getConfimationDialogue(
      context, alertDialogStatus, alertDialogMessage, choice) async {
    final appAlertDialog = AppAlertDialog();

    //dynamic alert dialogue, if allow to show yes and no option when choice is true
    //show ok option when choice is false
    // return true if press yes option, false when press no option
    bool userChoice = await appAlertDialog.showAsync(
      context: context,
      status: alertDialogStatus,
      message: alertDialogMessage,
      choice: choice,
    );
    // if alert dialogue press yes (return true) then exit
    if (userChoice) {
      _onLogoutButtonPressedAsync();
    }
  }

  Future<_DataModel> _getScreenDataAsync() async {
    final sharedPref = await SharedPreferences.getInstance();
    _dataModel = _DataModel();
    _dataModel.userEmail = sharedPref.getString(SharedPreferenceKeys.userEmail);
    return _dataModel;
  }

  Future<List<FriendDetailsModel>> _onLoadFriendAsync() async {
    // Call API.
    final repo = FriendListRepo();
    final apiResponse = await repo.FreindListAsync();
    return apiResponse.data;
  }

  @override
  void initState() {
    _futureBuilder = _getScreenDataAsync();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Home'),
        actions: [
          IconButton(
              icon: Icon(Icons.logout),
              tooltip: 'Logout',
              onPressed: () {
                _getConfimationDialogue(
                    context,
                    AppAlertDialogStatus.confirmation,
                    "Are you sure you want to log out ?",
                    true);
              }),
        ],
      ),
      body: FutureBuilder<_DataModel>(
        initialData: null,
        future: _futureBuilder,
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return Center(
              child: CircularProgressIndicator(),
            );
          } else if (snapshot.hasData) {
            return _content();
          }

          return Center(
            child: Text('Error'),
          );
        },
      ),
    );
  }

  Widget _content() {
    // this is to solve when user clicked the button quickly
    //it will called push to the same screen multiple times
    bool _firstPress = true;
    List<FriendDetailsModel> test;
    return Column(
      children: [
        Padding(
          padding: EdgeInsets.all(appConst.kDefaultPadding),
          child: Text('Hello ${_dataModel.userEmail}!'),
        ),
        Expanded(
          child: Center(
            child: ElevatedButton(
              child: Text('My Friend List'),
              onPressed: () async {
                // check for internet connection
                //if connected to the internet load from api and pass data to next screen
                bool connectionStatus = null;
                connectionStatus = await checkInternet.checkConnection(context);
                if (connectionStatus && _firstPress) {
                  _firstPress = false;
                  _onLoadFriendAsync().then((value) {
                    Navigator.of(context)
                        .pushNamed(AppRouter.myFriendList, arguments: value);
                    setState(() {});
                  });
                }
              },

              /* _onMyFriendListButtonPressed,*/
            ),
          ),
        ),
      ],
    );
  }
}

class _DataModel {
  String userEmail;
}
