import 'package:alcodes_on_board_flutter/app_router.dart';
import 'package:alcodes_on_board_flutter/constants/app_constants/app_constants.dart'
    as appConst;
import 'package:alcodes_on_board_flutter/models/response_models/friend_details_model.dart';
import 'package:flutter/material.dart';

/// TODO Issue on this screen:
/// - Show list of my friends.
/// - Click list item go to my friend detail page.

class MyFriendList extends StatefulWidget {
  final List<dynamic> data;

  //get the list from the repo
  MyFriendList({
    Key key,
    @required this.data,
  }) : super(key: key);

  @override
  _MyFriendListState createState() => _MyFriendListState();
}

class _MyFriendListState extends State<MyFriendList> {
  void _delteContact(FriendDetailsModel friend) {
    widget.data.remove(friend);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('My Friend List'),
        actions: <Widget>[],
      ),
      body: Container(
        padding: const EdgeInsets.all(appConst.kDefaultPadding),
        child: _getFriendDetails(context),
      ),
    );
  }

  Widget _getFriendDetails(BuildContext context) {
    if (widget.data.length == 0) {
      return _noFriendDeatails();
    } else {
      return ListView.builder(
          itemCount: widget.data.length,
          itemBuilder: (context, index) {
            return Card(
                elevation: 30,
                child: ListTile(
                  onTap: () {
                    Navigator.pushNamed(context, AppRouter.myFriendDetails,
                        arguments: widget.data[index]);
                  },
                  leading: CircleAvatar(
                    backgroundImage: NetworkImage(widget.data[index].avatar),
                    radius: 20.0,
                  ),
                  title: Text(widget.data[index].first_name +
                      " " +
                      widget.data[index].last_name),
                  trailing: ElevatedButton(
                      onPressed: () {
                        setState(() {
                          _delteContact(widget.data[index]);
                        });
                      },
                      child: Text("Delete")),
                ));
          });
    }
  }

  Widget _noFriendDeatails() {
    return Center(
      child: Container(
        child: Column(
          children: [
            new CircleAvatar(
                radius: 70.0,
                backgroundImage: NetworkImage(
                    "https://cdn.dribbble.com/users/521155/screenshots/3545281/emptystate.jpg?compress=1&resize=800x600")),
            SizedBox(height: appConst.kDefaultPadding),
            Text(
              'No Friends Available',
              textAlign: TextAlign.center,
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
          ],
        ),
      ),
    );
  }
}
