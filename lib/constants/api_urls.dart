class ApiUrls {
  static final String baseUrl = 'https://reqres.in/api/';
  static final String login = 'login';
  static final String friends = 'users?page=2';
}
