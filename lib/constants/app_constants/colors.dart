import 'package:flutter/material.dart';

const kPrimaryColor = Colors.blue;
const kAccentColor = Colors.blueGrey;
const kScreenBackgroundColor = Colors.white;

const kTextColor = Colors.black;
const kTextDangerColor = Color(0xFFDC3027);
const kTextSuccessColor = Color(0xFF00C851);
const kTextInfoColor = Color(0xFF4285F4);

const kAppBarBackgroundColor = Colors.blue;
const kAppBarTextColor = Colors.white;
