import 'dart:convert';

class FriendDetailsModel {
  int id;
  String email;
  String first_name;
  String last_name;
  String avatar;

  FriendDetailsModel({
    this.id,
    this.first_name,
    this.last_name,
    this.email,
    this.avatar,

  });

  factory FriendDetailsModel.fromJson(Map<String, dynamic> json) {
    return FriendDetailsModel(
      id: json["id"],
      first_name: json["first_name"],
      last_name: json["last_name"],
      email: json["email"],
      avatar: json["avatar"],
    );
  }


}
