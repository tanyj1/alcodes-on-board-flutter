import 'package:alcodes_on_board_flutter/constants/app_constants/app_constants.dart'
    as appConst;
import 'package:flutter/material.dart';

enum AppAlertDialogStatus { success, error, info, confirmation }

class AppAlertDialog {
  var _isDialogShowing = false;

  Future<bool> showAsync({
    @required BuildContext context,
    @required AppAlertDialogStatus status,
    @required String message,
    @required bool choice,
    @required String path,
    String title,
    bool barrierDismissible = true,
    String okButtonText,
  }) async {
    if (!_isDialogShowing) {
      _isDialogShowing = true;

      List<String> choiceMessage = [];

      if (choice == true) {
        var confirmText = 'No';
        var dismissText = "Yes";
        choiceMessage.add(confirmText);
        choiceMessage.add(dismissText);
      } else {
        var okText = 'OK';
        choiceMessage.add(okText);
      }
      PrimitiveWrapper data = new PrimitiveWrapper(false);

      await showDialog(
        context: context,
        barrierDismissible: barrierDismissible,
        builder: (context) {
          return AlertDialog(
            scrollable: true,
            title: _title(status, title),
            content: Text(message),
            actions: [
              for (int i = 0; i < choiceMessage.length; i++)
                _dialogueChoice(context, choice, choiceMessage, path, data)[i],
            ],
          );
        },
      );

      _isDialogShowing = false;

      if (data.dialogChoice) {
        return true;
      } else {
        return false;
      }
    }
  }

  void dismiss(BuildContext context) {
    if (_isDialogShowing) {
      _isDialogShowing = false;

      Navigator.of(context).pop();
    }
  }

  Widget _title(AppAlertDialogStatus status, String title) {
    switch (status) {
      case AppAlertDialogStatus.success:
        return _titleWithIcon(
          Icons.check_circle_outline_outlined,
          (title ?? 'Success'),
          appConst.kTextSuccessColor,
        );

      case AppAlertDialogStatus.confirmation:
        return _titleWithIcon(
          Icons.warning,
          (title ?? 'Log Out'),
          appConst.kTextInfoColor,
        );

      case AppAlertDialogStatus.error:
        return _titleWithIcon(
          Icons.cancel_outlined,
          (title ?? 'Oops!'),
          appConst.kTextDangerColor,
        );
      case AppAlertDialogStatus.info:
        return _titleWithIcon(
          Icons.info_outlined,
          (title ?? 'Info'),
          appConst.kTextInfoColor,
        );
      default:
        if (title != null) {
          return Text(title);
        }
    }

    return null;
  }

  Widget _titleWithIcon(IconData icon, String title, Color color) {
    return Row(
      children: [
        Icon(
          icon,
          color: color,
          size: 26,
        ),
        const SizedBox(width: 8),
        Text(
          title,
          style: TextStyle(
            color: color,
            fontSize: 20,
            fontWeight: FontWeight.bold,
          ),
        ),
      ],
    );
  }

  List<Widget> _dialogueChoice(BuildContext context, bool choice,
      List<String> choiceMessage, path, PrimitiveWrapper data) {
    List<Widget> list = [];
    if (choice == true) {
      for (int i = 0; i < choiceMessage.length; i++) {
        Widget widget = new FlatButton(
          onPressed: () {
            if (_isDialogShowing && choiceMessage[i] == "No") {
              dismiss(context);
            } else {
              dismiss(context);
              data.dialogChoice = true;
            }
          },
          child: Text(choiceMessage[i]),
        );
        list.add(widget);
      }
      return list;
    } else {
      Widget widget = new FlatButton(
        onPressed: () {
          if (_isDialogShowing) {
            dismiss(context);
          }
        },
        child: Text(choiceMessage[0]),
      );
      list.add(widget);
      return list;
    }
  }
}

class PrimitiveWrapper {
  bool dialogChoice;

  PrimitiveWrapper(this.dialogChoice);
}
