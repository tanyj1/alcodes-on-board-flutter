
import 'dart:async';

import 'package:alcodes_on_board_flutter/dialogs/app_alert_dialog.dart';
import 'package:data_connection_checker/data_connection_checker.dart';
import 'package:flutter/cupertino.dart';

class checkInternet{
  static StreamSubscription<DataConnectionStatus> listener;

  static Future<bool> checkConnection(BuildContext context) async {
    final appAlertDialog = AppAlertDialog();
    bool connectionStatus = null;
    listener = DataConnectionChecker().onStatusChange.listen((status) {});
    await DataConnectionChecker().connectionStatus.then((value) {
      if(value.toString() == "DataConnectionStatus.disconnected"){
        connectionStatus = false;

        appAlertDialog.showAsync(
          context: context,
          status: AppAlertDialogStatus.error,
          message: "You are not connected to the Internet",
          choice: false,
        );

      }else{
        connectionStatus = true;
      }
    });
    return connectionStatus;
  }


}
