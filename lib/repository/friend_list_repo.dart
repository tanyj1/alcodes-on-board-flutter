import 'package:alcodes_on_board_flutter/constants/api_urls.dart';
import 'package:alcodes_on_board_flutter/models/response_models/friend_details_model.dart';
import 'package:alcodes_on_board_flutter/utils/api_components/api_response.dart';
import 'package:dio/dio.dart';

class FriendListRepo {
  Future<ApiResponse<List<dynamic>>> FreindListAsync() async {
    try {
      final dio = Dio();
      dio.options.baseUrl = ApiUrls.baseUrl;
      List data;
      List<FriendDetailsModel> friendDetails = new List<FriendDetailsModel>();
      //get the whole json data from API
      final response = await dio.get(ApiUrls.friends);

      //get a specific section named "data" in the whole json
      data = response.data['data'];

      //convert json into object and add to a list
      data.forEach((element) {
        friendDetails.add(FriendDetailsModel.fromJson(element));
      });

      //return the list
      return ApiResponse<List<FriendDetailsModel>>(
        message: 'OK',
        data: friendDetails,
      );
    } on DioError catch (ex) {
      if (ex.response.statusCode == 400) {
        return Future.error(ex.response.data['error']);
      } else {
        return Future.error(ex);
      }
    } catch (ex) {
      return Future.error(ex);
    }
  }
}
